local S = minetest.get_translator("tntrun")

local function send_message(arena,num_str)
    arena_lib.HUD_send_msg_all("title", arena, num_str, 1,nil,0xFF0000)
    --arena_lib.HUD_send_msg_all(HUD_type, arena, msg, <duration>, <sound>, <color>)
end

local function tntplacing(arena)
  for k = 1, #arena.tnt_area_pos_1 do
    local pos1 = arena.tnt_area_pos_1[k]
    local pos2 = arena.tnt_area_pos_2[k]
    local x1 = pos1.x
    local x2 = pos2.x
    local y1 = pos1.y
    local y2 = pos2.y
    local z1 = pos1.z
    local z2 = pos2.z
    if x1 > x2 then
        local temp = x2
        x2 = x1
        x1 = temp
    end
    if y1 > y2 then
        local temp = y2
        y2 = y1
        y1 = temp
    end
    if z1 > z2 then
        local temp = z2
        z2 = z1
        z1 = temp
    end

    for x = x1,x2 do
        for y = y1,y2 do
            for z = z1,z2 do
                minetest.set_node({x=x,y=y,z=z}, {name="tntrun:tnt"})
            end
        end
    end
  end
end

arena_lib.on_load("tntrun", function(arena)
  tntplacing(arena)
  for pl_name, stats in pairs(arena.players) do
    local message = S('How it works:')
    minetest.chat_send_player(pl_name,message)
    message = minetest.colorize('#f47e1b', S('Punch TNTs to make them fall down'))
    minetest.chat_send_player(pl_name,message)
    message = minetest.colorize('#e6482e', S('But beware: TNTs will also fall down when you walk onto it. Keep moving!'))
    minetest.chat_send_player(pl_name,message)
    arena.players[pl_name].lives = arena.lives

  end
  send_message(arena,'3')
    minetest.after(1, function(arena)

        send_message(arena,'2')
        minetest.after(1, function(arena)
            send_message(arena,'1')
            minetest.after(1, function(arena)
                arena_lib.HUD_send_msg_all("title", arena, "Run!", 1,nil,0xE6482E)

                local item = ItemStack("tntrun:torch")


                for pl_name, stats in pairs(arena.players) do
                    local player = minetest.get_player_by_name(pl_name)
                    player:get_inventory():set_stack("main", 1, item)


                end



            end, arena)

        end, arena)

    end, arena)
end)

arena_lib.on_end("tntrun", function(arena)
  tntplacing(arena)
end)

-- arena_lib.on_start('tntrun', function(arena)
--     --give players the torch at the start
--     for pl_name, stats in pairs(arena.players) do
--         local player = minetest.get_player_by_name(pl_name)
--         local inv = player:get_inventory()
--         local stack = ItemStack("tntrun:torch")
--         inv:set_stack("main", 1, stack)
--     end
-- end)

arena_lib.on_death('tntrun', function(arena, p_name, reason)
    arena_lib.remove_player_from_arena(p_name, 1)
end)

minetest.register_on_joinplayer(function(player)
	local inv = player:get_inventory()
	local stack = ItemStack("tntrun:torch")
	local taken = inv:remove_item("main", stack)
end)

minetest.register_globalstep(function(dtime)
	for _, p_name in pairs(arena_lib.get_players_in_minigame("tntrun")) do
		if not arena_lib.is_player_spectating(p_name) then
			local player = minetest.get_player_by_name(p_name)
			local arena = arena_lib.get_arena_by_player(p_name)
			if player:get_pos().y <= arena.eliminate_height then
				arena_lib.remove_player_from_arena(p_name, 1)
			end
		end
	end
end)


if minetest.get_modpath("aes_xp") then
  arena_lib.on_celebration('tntrun', function(arena, winner_name)
    aes_xp.add_xp(winner_name, 25)
    minetest.chat_send_player(winner_name, "You got 25 XP for winning tntrun")
  end)
end
