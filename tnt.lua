local recursive_time = 0.1
local time_until_despawn = 5

local S = minetest.get_translator("tntrun")

minetest.register_entity("tntrun:tnt_falling", {
	initial_properties = {
		physical = true,
		collide_with_objects = true,
		pointable = true,
		collisionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
        selectionbox = {-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
		visual = "cube",
		visual_size = {x = 1, y = 1, z = 1},
		textures = {
			--[[animations aren't wokring!
			{name = "tnt_top_burning_animated.png",
			animation = {
	        type = "vertical_frames",
	        aspect_w = 16,
	        aspect_h = 16,
	        length = 1,
	      }
	  }]]"tnt_top_burning.png", "tnt_bottom.png", "tnt_side.png", "tnt_side.png", "tnt_side.png", "tnt_side.png"},
	},
	_timer = 0,
	on_step = function(self, dtime, moveresult)
		self.object:set_velocity({x = 0, y = -5, z = 0 })
		local pos = self.object:get_pos()
	    pos.y = pos.y - 1
		if minetest.registered_nodes[minetest.get_node(pos).name].walkable then
			self.object:remove()
		end
		if self._timer > time_until_despawn then
			self.object:remove()
		else
			self._timer = self._timer + dtime
		end
	end,
})

minetest.register_node("tntrun:tnt",{
  tiles = {"tnt_top.png", "tnt_bottom.png", "tnt_side.png"},
  groups = {cracky = 3},
  description = "TNT in tntrun",

  on_punch = function(pos, node, puncher)
    if puncher:get_wielded_item():get_name() == "tntrun:torch" then
      minetest.remove_node(pos)
      minetest.add_entity(pos, "tntrun:tnt_falling")
    end
  end,
  drops = "tntrun:tnt",
})

--[[minetest.register_node("tntrun:tnt_falling",{
  groups = {falling_node = 2, cracky = 3},
  tiles = {
    {
      name = "tnt_top_burning_animated.png",
      animation = {
        type = "vertical_frames",
        aspect_w = 16,
        aspect_h = 16,
        length = 1,
      }
    },
    "tnt_bottom.png", "tnt_side.png"
    },
  not_in_creative_inventory = true,
  drops = "tntrun:tnt",
  on_construct = function(pos)
    minetest.get_node_timer(pos):start(4)
    minetest.check_for_falling(pos)
  end,
  on_timer = function(pos)
    minetest.remove_node(pos)
    minetest.swap_node(pos, {name = "air"})
  end,
})]]

minetest.register_craftitem("tntrun:torch",{
	inventory_image = "default_torch_on_floor.png",
	description = S("Torch for TNT")
})


arena_lib.on_start("tntrun", function(arena)
  tnt_loop(arena)
end)

function tnt_loop(arena)
	if not arena.in_game then return end
	for pl_name, stats in pairs(arena.players) do
    	local player = minetest.get_player_by_name(pl_name)
    	local pos = player:get_pos()
    	pos.y = pos.y - 1
    	if minetest.get_node(pos).name == "tntrun:tnt" then
			pos.x = math.floor(pos.x+0.5)
			pos.z = math.floor(pos.z+0.5)
    		minetest.after(0.25, function()
				if minetest.get_node(pos).name ~= "tntrun:tnt" then
	   				return
				end
		  		minetest.remove_node(pos)
		  		minetest.add_entity(pos, "tntrun:tnt_falling")
			end)
		elseif minetest.get_node(pos).name == "air" then
			local sneak_distance = 0.3 --Change if minetest changes the sneak distance, works only for sneak distance bellow 0.5
			for x = -1,1 do
				for z = -1,1 do
					local posb = {x = pos.x+(x*sneak_distance), y = pos.y, z = pos.z+(z*sneak_distance)}
					if minetest.get_node(posb).name == "tntrun:tnt" then
						posb.x = math.floor(posb.x+0.5)
						posb.z = math.floor(posb.z+0.5)
			    		minetest.after(0.5, function() --It's wanted that it's longer
							if minetest.get_node(posb).name ~= "tntrun:tnt" then
				   				return
							end
					  		minetest.remove_node(posb)
					  		minetest.add_entity(posb, "tntrun:tnt_falling")
						end)
					end
				end
			end
		end
	end
	minetest.after(recursive_time, function() tnt_loop(arena) end)
end
