local S = minetest.get_translator("tntrun")
tntrun = {}
dofile(minetest.get_modpath("tntrun") .. "/settings.lua")

local player_jump = tntrun.player_jump
local player_speed = tntrun.player_speed


arena_lib.register_minigame("tntrun", {
	name = "TntRun!",
	icon = "tntrun_icon.png",
	celebration_time = 3,
	load_time = 3,
	prefix = "[Sp] ",
	show_minimap = true,
	properties = {
    	tnt_area_pos_1 = {{x = 0, y = 0, z = 0}},
    	tnt_area_pos_2 = {{x = 0, y = 0, z = 0}},
		eliminate_height = 0, --bellow this height you get eliminated
	},
	in_game_physics = {
    	speed = player_speed,
    	jump = player_jump,
  	},
	disabled_damage_types = {"punch", "fall"},
	hotbar = {
    	slots = 1,
    	background_image = "tntrun_gui_hotbar.png"
	},
	can_drop = false,
})

minetest.register_privilege("tntrun_admin", S("Needed for Tntrun"))

dofile(minetest.get_modpath("tntrun") .. "/nodes.lua")
dofile(minetest.get_modpath("tntrun") .. "/auto.lua")
dofile(minetest.get_modpath("tntrun") .. "/tnt.lua")



arena_lib.on_enable("tntrun", function(arena, p_name)
  if #arena.tnt_area_pos_1 ~= #arena.tnt_area_pos_2 then
    minetest.chat_send_player(p_name,"Missing params in the positions")
    return false
  end
  return true
end)
